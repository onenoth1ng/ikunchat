package com.hcb.im;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.hcb.im.controller.fragment.ChatFragment;
import com.hcb.im.model.Model;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMOptions;
import com.hyphenate.easeui.EaseIM;
import com.hyphenate.easeui.domain.EaseAvatarOptions;

import java.util.List;

public class IMApplication extends Application {
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();

        //初始化Ease环信
        EMOptions options = new EMOptions();
        options.setAppKey("1144220921104163#ikunchat");
        // 其他 EMOptions 配置。
        options.setAcceptInvitationAlways(false);
        options.setAutoAcceptGroupInvitation(false);
        EMClient.getInstance().init(this, options);




        //初始化数据模型层类
        Model.getInstance().init(this);

        //初始化全局上下文
        mContext = this;

        //初始化EaseKit
        if (EaseIM.getInstance().init(mContext,options)){
            EMClient.getInstance().setDebugMode(true);
        }

    }



    //获取全局上下文对象
    public static Context getGlobalApplication() {
        return mContext;
    }
}
