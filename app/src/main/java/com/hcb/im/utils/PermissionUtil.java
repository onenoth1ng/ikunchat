package com.hcb.im.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class PermissionUtil {

    //检查多个权限，返回true表示 已完全 开启权限，返回false表示没有 完全 开启权限
    public static boolean checkPermission(Activity activity, String[] permissions, int requestCode) {
        //只有在 Android 6.0之后才会进行动态权限管理
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int check = PackageManager.PERMISSION_GRANTED;
            for (String permission : permissions) {
                check = ContextCompat.checkSelfPermission(activity, permission);
                if (check != PackageManager.PERMISSION_GRANTED) {
                    break;
                }
            }

            //未开启权限，则请求系统弹窗，让用户选择是否立即开启权限
            if (check != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(activity,permissions,requestCode);
                return false;
            }
        }
        return true;
    }

    //检查权限结果数组，返回true表示都已经获得了权限，返回false表示还有没有会的权限的
    public static boolean checkGrant(int[] grantResults) {
        if (grantResults != null){
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED){
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
