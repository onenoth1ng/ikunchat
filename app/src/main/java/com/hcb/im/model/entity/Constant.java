package com.hcb.im.model.entity;

import android.content.Intent;

//全局的常量类
public class Constant {

    public static final String CONTACTS_CHANGED = "contact_changed"; // 发送联系人变化的的广播

    public static final String CONTACTS_INVITE_CHANGED = "contact_invite_changed";//联系人邀请信息变化的广播
    public static final String GROUP_INVITE_CHANGED = "group_invite_changed"; //群邀请信息变化的广播
    public static final String GROUP_ID = "group_id";
    public static final String QUIT_GROUP = "quit_group"; //退群广播
}
