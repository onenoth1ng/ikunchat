package com.hcb.im.model.dao;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.hcb.im.model.db.UserAccountDB;
import com.hcb.im.model.entity.UserInfo;

//账号数据库的操作类
public class UserAccountDao {

    private final UserAccountDB mDBHelper;

    public UserAccountDao(Context context) {
        mDBHelper = new UserAccountDB(context);
    }

    public void addAccount(UserInfo userInfo){
        //获取数据库对象
        SQLiteDatabase db = mDBHelper.getReadableDatabase();
        //执行添加操作
        ContentValues values = new ContentValues();
        values.put(UserAccountTable.COL_NAME,userInfo.getName());
        values.put(UserAccountTable.COL_HXID,userInfo.getHxid());
        values.put(UserAccountTable.COL_NICK,userInfo.getNick());
        values.put(UserAccountTable.COL_PHOTO,userInfo.getPhoto());
        db.replace(UserAccountTable.TABLE_NAME,null,values);
    }

    @SuppressLint("Range")
    public UserInfo getAccountByHXid(String hxId){
        //获取数据库对象
        SQLiteDatabase db = mDBHelper.getReadableDatabase();
        //执行查询语句
        String sql = "select * from " + UserAccountTable.TABLE_NAME + " where " + UserAccountTable.COL_HXID + " = ? ";
        Cursor cursor = db.rawQuery(sql, new String[]{hxId});
        UserInfo userInfo = new UserInfo();
        if (cursor.moveToNext()){
            userInfo.setName(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_NAME)));
            userInfo.setHxid(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXID)));
            userInfo.setNick(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_NICK)));
            userInfo.setPhoto(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_PHOTO)));
        }
        //关闭资源
        db.close();
        //返回数据
        return userInfo;
    }
}
