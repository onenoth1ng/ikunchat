package com.hcb.im.model.entity;


public class PickContactInfo {
    private UserInfo userInfo;  //联系人的信息
    private boolean isChecked; //这个联系人是否呗选中

    public PickContactInfo(UserInfo userInfo, boolean isChecked) {
        this.userInfo = userInfo;
        this.isChecked = isChecked;
    }

    public PickContactInfo() {
    }

    @Override
    public String toString() {
        return "PickContactInfo{" +
                "userInfo=" + userInfo +
                ", isChecked=" + isChecked +
                '}';
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
