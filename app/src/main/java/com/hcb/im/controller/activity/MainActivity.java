package com.hcb.im.controller.activity;


import android.app.FragmentManager;
import android.os.Bundle;
import android.widget.RadioGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.hcb.im.R;
import com.hcb.im.controller.fragment.ChatFragment;
import com.hcb.im.controller.fragment.ContactListFragment;
import com.hcb.im.controller.fragment.DemoContactsFragment;
import com.hcb.im.controller.fragment.SettingFragment;

public class MainActivity extends FragmentActivity {
    private RadioGroup rg_main;
    private ChatFragment chatFragment;
    private ContactListFragment contactListFragment;
    private SettingFragment settingFragment;

    private DemoContactsFragment demoContactsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //初始化页面
        initView();
        //初始化数据 fragment
        initData();
        //初始化监听
        initListener();

    }

    private void initListener() {
        //RadioGroup的选择点击事件
        rg_main.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                Fragment fragment = null;
                switch (i){
                    //会话列表页面
                    case R.id.rb_main_conversation:
                        fragment = chatFragment;
                        break;

                    //联系人列表页面
                    case R.id.rb_main_contact:
                        fragment = contactListFragment;
                        break;

                    //设置列表页面
                    case R.id.rb_main_setting:
                        fragment = settingFragment;
                        break;

                }

                //实现fragment切换的方法
                switchFragment(fragment);
            }
        });

        //默认选择 会话 窗口
        rg_main.check(R.id.rb_main_conversation);
    }

    private void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fl_main,fragment).commit();
        /*FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fl_main, fragment).commit();*/
    }

    private void initData() {
        //创建三个Fragment对象
        chatFragment = new ChatFragment();
        contactListFragment= new ContactListFragment();
        settingFragment = new SettingFragment();

        demoContactsFragment = new DemoContactsFragment();

    }

    private void initView() {
        rg_main = findViewById(R.id.rg_main);
    }
}