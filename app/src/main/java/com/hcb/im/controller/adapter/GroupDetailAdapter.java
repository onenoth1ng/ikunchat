package com.hcb.im.controller.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hcb.im.R;
import com.hcb.im.model.entity.UserInfo;
import com.hyphenate.easeui.modules.conversation.interfaces.IConversationListLayout;

import java.util.ArrayList;
import java.util.List;

public class GroupDetailAdapter extends BaseAdapter {
    private Context mContext;
    private boolean mIsCanModify;
    private List<UserInfo> mUsers = new ArrayList<>();
    private boolean mIsDeleteModel = false;//群成员的删除模式

    //获取当前的删除模式
    public boolean ismIsDeleteModel() {
        return mIsDeleteModel;
    }

    //设置当前的删除模式
    public void setmIsDeleteModel(boolean mIsDeleteModel) {
        this.mIsDeleteModel = mIsDeleteModel;
    }

    private OnGroupDetailListener mOnGroupDetailListener;

    public GroupDetailAdapter(Context context, boolean isCanModify, OnGroupDetailListener onGroupDetailListener) {
        mContext = context;
        mIsCanModify = isCanModify;
        mOnGroupDetailListener = onGroupDetailListener;
    }

    //刷新数据的方法
    public void refresh(List<UserInfo> users){
        if (users != null && users.size() >= 0){
            mUsers.clear();

            //添加 加号 和 减号 （拉人，踢人）
            initUsers();

            mUsers.addAll(0,users);

        }
        notifyDataSetChanged();
    }

    private void initUsers() {
        UserInfo add = new UserInfo("add");
        UserInfo del = new UserInfo("del");

        mUsers.add(del);
        mUsers.add(0,add);
    }

    @Override
    public int getCount() {
        return mUsers == null ? 0 : mUsers.size();
    }

    @Override
    public Object getItem(int i) {
        return mUsers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        //创建或 获取 ViewHolder
        ViewHolder holder = null;
        if (convertView == null){
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.item_groupdetail,null);
            holder.photo = convertView.findViewById(R.id.iv_group_detail_photo);
            holder.delete = convertView.findViewById(R.id.iv_group_detail_delete);
            holder.name = convertView.findViewById(R.id.tv_group_detail_name);

            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        //获取当前item数据
        UserInfo userInfo = mUsers.get(i);
        //展示当前item数据
        if (mIsCanModify){//允许修改群信息

            //布局的处理
            if (i == getCount() -1 ){ //减号的处理
                //删除模式的判断
                if (mIsDeleteModel){//是删除模式
                    convertView.setVisibility(View.INVISIBLE);
                }else{// 不是删除模式
                    convertView.setVisibility(View.VISIBLE);
                    //图片设置为 减号
                    holder.photo.setImageResource(R.drawable.em_smiley_minus_btn_pressed);
                    holder.delete.setVisibility(View.GONE);
                    holder.name.setVisibility(View.INVISIBLE);
                }

            }else if (i == getCount() -2 ){//加号的处理,convertView部分的处理
                //删除模式的判断
                if (mIsDeleteModel){//是删除模式
                    convertView.setVisibility(View.INVISIBLE);
                }else{// 不是删除模式
                    convertView.setVisibility(View.VISIBLE);
                    //图片设置为 加号
                    holder.photo.setImageResource(R.drawable.em_smiley_add_btn_pressed);
                    holder.delete.setVisibility(View.GONE);
                    holder.name.setVisibility(View.INVISIBLE);
                }

            }else{//其他成员的处理
                convertView.setVisibility(View.VISIBLE);
                holder.name.setVisibility(View.VISIBLE);
                holder.name.setText(userInfo.getName());
                holder.photo.setImageResource(R.drawable.ikunji);
                if (mIsDeleteModel){//是删除模式
                    holder.delete.setVisibility(View.VISIBLE);
                }else{
                    holder.delete.setVisibility(View.GONE);
                }
            }

            //点击事件的处理
            if (i == getCount() -1 ){//减号的点击事件处理
                holder.photo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!mIsDeleteModel){
                            mIsDeleteModel = true;
                            notifyDataSetChanged();
                        }
                    }
                });
            }else if (i == getCount() -2 ){//加号的点击事件处理
                holder.photo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mOnGroupDetailListener.onAddMembers();
                    }
                });
            }else {
                holder.delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mOnGroupDetailListener.onDeleteMembers(userInfo);
                    }
                });
            }

        }else{//不允许修改群信息
            if (i == getCount() - 1 || i == getCount() - 2){
                //减号 和 加号 隐藏掉
                convertView.setVisibility(View.GONE);
                holder.delete.setVisibility(View.GONE);
                holder.name.setText(userInfo.getName());
                holder.photo.setImageResource(R.drawable.ikunji);

            }
        }
        //返回convertView;
        return convertView;
    }

    private class ViewHolder{
        private ImageView photo;
        private ImageView delete;
        private TextView name;
    }

    public interface OnGroupDetailListener{
        //添加群成员的方法
        void onAddMembers();
        //删除裙成员的方法
        void onDeleteMembers(UserInfo user);
    }
}
