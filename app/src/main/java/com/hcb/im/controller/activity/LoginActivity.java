package com.hcb.im.controller.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hcb.im.R;
import com.hcb.im.model.Model;
import com.hcb.im.model.dao.UserAccountDao;
import com.hcb.im.model.entity.UserInfo;
import com.hcb.im.utils.ToastUtil;
import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.hyphenate.exceptions.HyphenateException;


//登录页面
public class LoginActivity extends Activity {
    private EditText et_login_pwd;
    private EditText et_login_name;
    private Button bt_login_login;
    private Button bt_login_regist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //初始化控件
        initView();
        //初始化监听
        initListener();


    }

    private void initListener() {
        bt_login_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
        bt_login_regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                regist();
            }
        });
    }

    //登录功能的实现
    private void login() {
        //获取输入的用户名和密码
        String loginName = et_login_name.getText().toString();
        String loginPwd = et_login_pwd.getText().toString();

        //对输入的用户名密码进行校验
        if (TextUtils.isEmpty(loginName) || TextUtils.isEmpty(loginPwd)){
            ToastUtil.show(this,"用户名或密码不能为空");
            return;
        }

        //登录逻辑处理
        Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
            @Override
            public void run() {
                //去环信服务器登录
                EMClient.getInstance().login(loginName, loginPwd, new EMCallBack() {
                    //登录成功的回调
                    @Override
                    public void onSuccess() {
                        //对数据模型层进行处理
                        Model.getInstance().loginSuccess(new UserInfo(loginName));

                        //保存用户账号的信息到本地的数据库
                        UserAccountDao userAccountDao = Model.getInstance().getUserAccountDao();
                        userAccountDao.addAccount(new UserInfo(loginName));
                        //提示登录成功
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //提示登录成功
                                ToastUtil.show(LoginActivity.this,"登录成功");
                                //跳转到会话页面
                                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                                startActivity(intent);
                                //当前页面被销毁
                                finish();
                            }
                        });
                    }

                    //登录失败的回调
                    @Override
                    public void onError(int code, String error) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //提示登录失败
                                ToastUtil.show(LoginActivity.this,"登陆失败(密码错误)"+ error);
                            }
                        });
                    }

                    //登录过程中的处理
                    @Override
                    public void onProgress(int progress, String status) {
                        EMCallBack.super.onProgress(progress, status);
                    }
                });
            }
        });
    }

    private void regist() {
        //获取用户名密码
        String registName = et_login_name.getText().toString();
        String registPwd = et_login_pwd.getText().toString();
        //校验输入的用户名密码
        if (TextUtils.isEmpty(registName) || TextUtils.isEmpty(registPwd)){
            ToastUtil.show(this,"用户名或密码不能为空");
            return;
        }
        //注册到服务器
        Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
            @Override
            public void run() {
                //去环信服务器注册
                try {
                    EMClient.getInstance().createAccount(registName,registPwd);
                    //更新页面显示
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ToastUtil.show(LoginActivity.this,"注册成功");
                        }
                    });
                } catch (HyphenateException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ToastUtil.show(LoginActivity.this,"注册失败");
                        }
                    });
                }
            }
        });
    }

    private void initView() {
        et_login_name = findViewById(R.id.et_login_name);
        et_login_pwd = findViewById(R.id.et_login_pwd);
        bt_login_login = findViewById(R.id.bt_login_login);
        bt_login_regist = findViewById(R.id.bt_login_regist);
    }

}