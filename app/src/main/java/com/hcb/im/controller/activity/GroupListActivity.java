package com.hcb.im.controller.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.hcb.im.R;
import com.hcb.im.controller.adapter.GroupListAdapter;
import com.hcb.im.model.Model;
import com.hcb.im.model.entity.Constant;
import com.hcb.im.utils.ToastUtil;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMGroup;
import com.hyphenate.easeui.constants.EaseConstant;
import com.hyphenate.exceptions.HyphenateException;

import java.util.List;

public class GroupListActivity extends AppCompatActivity {
    private ListView lv_groupList;
    private GroupListAdapter adapter;
    private List<EMGroup> mGroups;
    private LinearLayout ll_grouplist;
    private LocalBroadcastManager mLBM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_list);

        initView();

        initData();

        initListener();

    }

    private void initListener() {
        //listView条目的点击事件
        lv_groupList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (i == 0){
                    return;
                }
                Intent intent = new Intent(GroupListActivity.this, ChatActivity.class);
                //设置 会话类型
                intent.putExtra(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_GROUP);
                //设置 群Id
                intent.putExtra(EaseConstant.EXTRA_CONVERSATION_ID, mGroups.get(i-1).getGroupId());
                intent.putExtra("group_name", mGroups.get(i-1).getGroupName());
                startActivity(intent);
            }
        });


        //点击了新建群的页面
        ll_grouplist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GroupListActivity.this, NewGroupActivity.class);
                startActivity(intent);
            }
        });
        // 注册退群广播
        BroadcastReceiver ExitGroupReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //结束当前页面
                adapter.notifyDataSetChanged();
            }
        };
        mLBM.registerReceiver(ExitGroupReceiver,new IntentFilter(Constant.QUIT_GROUP));
    }

    private void initData() {
        adapter = new GroupListAdapter(this);
        lv_groupList.setAdapter(adapter);
        //从环信服务器获取所有群的信息
        getGroupsFromServer();

        mLBM = LocalBroadcastManager.getInstance(GroupListActivity.this);
    }

    private void getGroupsFromServer() {
        Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    //从网络获取群的信息
                    mGroups = EMClient.getInstance().groupManager().getJoinedGroupsFromServer();
                    //更新页面
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //调用构造器里面的方法
                            adapter.refresh(mGroups);
                        }
                    });
                } catch (HyphenateException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ToastUtil.show(GroupListActivity.this,"加载群失败");
                        }
                    });
                }
            }
        });
    }

    private void initView() {
        lv_groupList = findViewById(R.id.lv_groupList);

        //添加头布局
        View headerView = View.inflate(this, R.layout.header_grouplist, null);
        lv_groupList.addHeaderView(headerView);

        ll_grouplist = headerView.findViewById(R.id.ll_grouplist);
    }

    //每次这个页面可见的时候，刷新页面
    @Override
    protected void onResume() {
        super.onResume();
        adapter.refresh(mGroups);
    }
}