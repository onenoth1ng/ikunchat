package com.hcb.im.controller.activity;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.hcb.im.R;
import com.hcb.im.model.Model;
import com.hcb.im.utils.ToastUtil;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMGroupManager;
import com.hyphenate.chat.EMGroupOptions;
import com.hyphenate.easeui.constants.EaseConstant;
import com.hyphenate.exceptions.HyphenateException;

public class NewGroupActivity extends AppCompatActivity {
    private EditText et_new_group_name;
    private EditText et_new_group_desc;
    private CheckBox cb_new_group_public;
    private CheckBox cb_new_group_invite;
    private Button bt_new_group_create;
    private ActivityResultLauncher<Intent> register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_group);

        initView();

        initListener();

        register = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            Intent data = result.getData();
            try {
                String[] members = data.getStringArrayExtra("members");
                //createGroup(data.getStringArrayExtra("members"));
                Log.d("OneNoth1ng", String.valueOf(members.length));
                createGroup(members);
            }catch (Exception e){
                e.printStackTrace();
            }

        });

    }

    private void initListener() {
        //创建按钮的 按钮 点击事件
        bt_new_group_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //跳转到 选择联系人页面
                /*Intent intent = new Intent(NewGroupActivity.this, PickContactActivity.class);
                startActivityForResult(intent, 1);*/

                register.launch(new Intent(NewGroupActivity.this,PickContactActivity.class));
            }
        });
    }


    //创建群 的 方法
    private void createGroup(String[] members) {
        Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
            @Override
            public void run() {
                //去环信服务器创建群
                //群名  群描述   哪些人  原因  群的设置
                EMGroupOptions options = new EMGroupOptions();
                //最多200人
                options.maxUsers = 200;
                EMGroupManager.EMGroupStyle groupStyle = null;
                //群设置的 四种模式
                if (cb_new_group_public.isChecked()){//公开群
                    if (cb_new_group_invite.isChecked()){//开放群成员可以邀请人
                        groupStyle = EMGroupManager.EMGroupStyle.EMGroupStylePublicOpenJoin;
                    }else{
                        groupStyle = EMGroupManager.EMGroupStyle.EMGroupStylePublicJoinNeedApproval;
                    }
                }else{//不公开群
                    if (cb_new_group_invite.isChecked()){// 开放群邀请
                        groupStyle = EMGroupManager.EMGroupStyle.EMGroupStylePrivateMemberCanInvite;
                    }else{
                        groupStyle = EMGroupManager.EMGroupStyle.EMGroupStylePrivateOnlyOwnerInvite;
                    }
                }
                options.style = groupStyle;
                options.inviteNeedConfirm = true;
                try {
                    EMClient.getInstance().groupManager().createGroup(et_new_group_name.getText().toString()
                            , et_new_group_desc.getText().toString()
                            , members
                            , "Ikun聚集地"
                            , options);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ToastUtil.show(NewGroupActivity.this,"创建群成功");
                            //结束当前页面
                            finish();
                        }
                    });
                } catch (HyphenateException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ToastUtil.show(NewGroupActivity.this,"创建群失败");
                        }
                    });
                }
            }
        });
    }


    private void initView() {
        et_new_group_name = findViewById(R.id.et_new_group_name);
        et_new_group_desc = findViewById(R.id.et_new_group_desc);
        cb_new_group_public = findViewById(R.id.cb_new_group_public);
        cb_new_group_invite = findViewById(R.id.cb_new_group_invite);
        bt_new_group_create = findViewById(R.id.bt_new_group_create);

    }
}