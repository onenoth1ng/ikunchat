package com.hcb.im.controller.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hcb.im.R;
import com.hcb.im.model.Model;
import com.hcb.im.model.entity.UserInfo;
import com.hcb.im.utils.ToastUtil;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMContactManager;
import com.hyphenate.exceptions.HyphenateException;

public class AddContactActivity extends AppCompatActivity {
    private TextView tv_add_find;
    private EditText et_add_name;
    private RelativeLayout rl_add;
    private TextView tv_add_name;
    private Button bt_add_add;

    private UserInfo userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        //初始化view
        initView();
        //初始化监听
        initListener();
    }

    private void initListener() {
        //查找
        tv_add_find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                find();
            }
        });

        //添加按钮的点击事件处理
        bt_add_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add();
            }
        });
    }

    //查找按钮的处理
    private void find() {
        //获取输入的用户名称
        String name = et_add_name.getText().toString();
        //校验输入的用户名称
        if (TextUtils.isEmpty(name)) {
            ToastUtil.show(AddContactActivity.this, "输入的用户名称不能为空");
            return;
        }
        //去服务器判断当前用户是否存在
        Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
            @Override
            public void run() {
                //去服务器判断当前用户是否存在
                userInfo = new UserInfo(name);
                //更新UI显示
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //将布局设为可见
                        rl_add.setVisibility(View.VISIBLE);
                        //设置昵称
                        tv_add_name.setText(name);
                    }
                });
            }
        });
    }

    //添加按钮的处理
    private void add() {
        Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
            @Override
            public void run() {
                //去环信服务器添加好友
                EMContactManager manager = EMClient.getInstance().contactManager();
                try {
                    manager.addContact(userInfo.getName(), "小黑子请求添加好友");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ToastUtil.show(AddContactActivity.this, "发送好友请求成功");
                        }
                    });
                } catch (HyphenateException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ToastUtil.show(AddContactActivity.this, "发送好友请求失败" + e.toString());
                        }
                    });
                }
            }
        });
    }

    private void initView() {
        tv_add_find = findViewById(R.id.tv_add_contact_search);
        et_add_name = findViewById(R.id.et_add_contact_name);
        rl_add = findViewById(R.id.rl_add_contact);
        tv_add_name = findViewById(R.id.tv_add_contact_name);
        bt_add_add = findViewById(R.id.bt_add_contact_add);

    }
}