package com.hcb.im.controller.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.window.SplashScreen;

import com.hcb.im.R;
import com.hcb.im.model.Model;
import com.hcb.im.model.entity.UserInfo;
import com.hyphenate.chat.EMClient;

public class splashActivity extends Activity {

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            //如果当前activity已经退出，那么就不处理handler中的消息，直接返回
            if (isFinishing()) {
                return;
            }
            //判断进入主页面 还是 登录页面
            toMainOrLogin();
        }
    };

    //判断进入主页面 还是 登录页面
    private void toMainOrLogin() {
        Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
            @Override
            public void run() {
                //判断该账号 是否已经登录过
                if (EMClient.getInstance().isLoggedInBefore()) {//登录过了，直接登录主界面(会话)
                    //获取当前登录用户的信息
                    UserInfo account = Model.getInstance().getUserAccountDao().getAccountByHXid(EMClient.getInstance().getCurrentUser());
                    if (account == null){
                        //没登录过，进入登录界面
                        Intent intent = new Intent(splashActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }else{
                        //登录成功之后的方法
                        Model.getInstance().loginSuccess(account);
                        //跳转主界面
                        Intent intent = new Intent(splashActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                } else {
                    //没登录过，进入登录界面
                    Intent intent = new Intent(splashActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
                //结束当前页面,避免返回到欢迎页面
                finish();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);



        //发送1.5s的延时消息
        handler.sendMessageDelayed(Message.obtain(), 1500);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //销毁消息
        handler.removeCallbacksAndMessages(null);
    }
}