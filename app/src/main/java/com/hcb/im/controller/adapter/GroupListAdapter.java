package com.hcb.im.controller.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hcb.im.R;
import com.hyphenate.chat.EMGroup;

import java.util.ArrayList;
import java.util.List;

public class GroupListAdapter extends BaseAdapter {
    private Context mContext;
    private List<EMGroup> mGroups = new ArrayList<>();

    public GroupListAdapter(Context context) {
        mContext = context;
    }

    //刷新的方法，刷新群组的信息，展示群组
    public void refresh(List<EMGroup> groups){
        if (groups != null && groups.size() >=0){
            //先清空原来的数据
            mGroups.clear();

            //添加数据
            mGroups.addAll(groups);

            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        return mGroups == null ? 0 : mGroups.size();
    }

    @Override
    public Object getItem(int i) {
        return mGroups.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        //创建 或 获取 ViewHolder
        ViewHolder holder = null;
        if(convertView == null){
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.item_grouplist,null);
            holder.name = convertView.findViewById(R.id.tv_grouplist_name);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        //获取当前item数据
        EMGroup emGroup = mGroups.get(i);
        //展示item数据
        holder.name.setText(emGroup.getGroupName());
        //返回数据
        return convertView;
    }

    private class ViewHolder{
        TextView name;
    }
}
