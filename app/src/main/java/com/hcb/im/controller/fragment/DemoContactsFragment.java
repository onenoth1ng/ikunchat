package com.hcb.im.controller.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.hcb.im.R;
import com.hcb.im.controller.activity.AddContactActivity;
import com.hcb.im.controller.activity.ChatActivity;
import com.hcb.im.utils.ToastUtil;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.constants.EaseConstant;
import com.hyphenate.easeui.modules.contact.EaseContactListFragment;
import com.hyphenate.easeui.modules.conversation.EaseConversationListFragment;
import com.hyphenate.easeui.modules.conversation.delegate.EaseSystemMsgDelegate;
import com.hyphenate.easeui.utils.EaseCommonUtils;
import com.hyphenate.easeui.widget.EaseImageView;
import com.hyphenate.easeui.widget.EaseSearchTextView;

import java.util.List;

//创建会话列表页面
public class DemoContactsFragment extends EaseContactListFragment {



    @Override
    public void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);


    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
