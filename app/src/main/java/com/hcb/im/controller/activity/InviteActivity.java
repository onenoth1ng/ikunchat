package com.hcb.im.controller.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.hcb.im.R;
import com.hcb.im.controller.adapter.InviteAdapter;
import com.hcb.im.model.Model;
import com.hcb.im.model.entity.Constant;
import com.hcb.im.model.entity.InvationInfo;
import com.hcb.im.utils.ToastUtil;
import com.hyphenate.chat.EMClient;
import com.hyphenate.exceptions.HyphenateException;

import java.util.List;

//邀请信息列表页面
public class InviteActivity extends AppCompatActivity {
    private ListView lv_invite;
    private InviteAdapter adapter;
    private LocalBroadcastManager mLBM;
    private InviteAdapter.OnInviteListener mOnInviteListener = new InviteAdapter.OnInviteListener() {
        //联系人接受按钮的点击事件
        @Override
        public void onAccept(InvationInfo invationInfo) {
            //通知环信服务器，点击了接受按钮
            Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        EMClient.getInstance().contactManager().acceptInvitation(invationInfo.getUserInfo().getHxid());

                        //数据库更新，添加联系人
                        Model.getInstance().getDbManager().getInviteTableDao().updateInvitationStatus(InvationInfo.InvitationStatus.INVITE_ACCEPT, invationInfo.getUserInfo().getHxid());
                        //页面发生变化
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //页面发生变化
                                ToastUtil.show(InviteActivity.this, "小黑子露出鸡脚了吧");
                                //刷新页面
                                refresh();
                            }
                        });
                    } catch (HyphenateException e) {
                        e.printStackTrace();
                        ToastUtil.show(InviteActivity.this, "接受小黑子邀请失败了" + e);
                    }
                }
            });
        }

        //联系人拒绝按钮的点击事件
        @Override
        public void onReject(InvationInfo invationInfo) {
            Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        //环信服务器发送拒绝邀请
                        EMClient.getInstance().contactManager().declineInvitation(invationInfo.getUserInfo().getHxid());
                        //数据库变化
                        Model.getInstance().getDbManager().getInviteTableDao().removeInvitation(invationInfo.getUserInfo().getHxid());
                        //页面变化
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //页面变化
                                ToastUtil.show(InviteActivity.this, "你拒绝了小黑子");
                                //刷新页面
                                refresh();
                            }
                        });
                    } catch (HyphenateException e) {
                        e.printStackTrace();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ToastUtil.show(InviteActivity.this, "拒绝失败了");
                            }
                        });
                    }
                }
            });
        }


        //接受邀请按钮的处理
        @Override
        public void onInviteAccept(InvationInfo invationInfo) {
            Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    //向环信服务器发送接受群邀请的事件
                    try {
                        EMClient.getInstance().groupManager().acceptInvitation(invationInfo.getGroupInfo().getGroupId(),
                                invationInfo.getGroupInfo().getInvatePerson());
                        //本地数据库更新
                        invationInfo.setStatus(InvationInfo.InvitationStatus.GROUP_ACCEPT_INVITE);
                        Model.getInstance().getDbManager().getInviteTableDao().addInvitation(invationInfo);
                        //内存数据变化
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ToastUtil.show(InviteActivity.this, " 你接受了群邀请");
                                //刷新页面
                                refresh();
                            }
                        });
                    } catch (HyphenateException e) {
                        e.printStackTrace();
                        ToastUtil.show(InviteActivity.this, "发生了错误！");
                    }
                }
            });
        }

        //拒绝邀请按钮的处理
        @Override
        public void onInviteReject(InvationInfo invationInfo) {
            Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        //去环信服务器 告诉服务器， 我拒绝了
                        EMClient.getInstance().groupManager().declineInvitation(invationInfo.getGroupInfo().getGroupId(),
                                invationInfo.getGroupInfo().getInvatePerson(),
                                "拒绝了你的邀请");
                        //更新本地数据库
                        invationInfo.setStatus(InvationInfo.InvitationStatus.GROUP_REJECT_INVITE);
                        Model.getInstance().getDbManager().getInviteTableDao().addInvitation(invationInfo);
                        //更新内存数据
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ToastUtil.show(InviteActivity.this,"拒绝邀请成功");
                                //刷新页面
                                refresh();
                            }
                        });
                    } catch (HyphenateException e) {
                        e.printStackTrace();
                        ToastUtil.show(InviteActivity.this, "发生了错误！");
                    }
                }
            });

        }

        //接受别人申请入群按钮的处理
        @Override
        public void onApplicationAccept(InvationInfo invationInfo) {
            Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        //去环信服务器 告诉服务器， 我接受申请了
                        EMClient.getInstance().groupManager().acceptApplication(invationInfo.getGroupInfo().getInvatePerson(),invationInfo.getGroupInfo().getGroupId());
                        //更新本地数据库
                        invationInfo.setStatus(InvationInfo.InvitationStatus.GROUP_ACCEPT_APPLICATION);
                        Model.getInstance().getDbManager().getInviteTableDao().addInvitation(invationInfo);
                        //更新内存数据
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ToastUtil.show(InviteActivity.this,invationInfo.getGroupInfo().getInvatePerson()+"接受了你的申请");
                                //刷新页面
                                refresh();
                            }
                        });
                    } catch (HyphenateException e) {
                        e.printStackTrace();
                        ToastUtil.show(InviteActivity.this, "发生了错误！");
                    }
                }
            });

        }

        //拒绝别人申请入群按钮的处理
        @Override
        public void onApplicationReject(InvationInfo invationInfo) {
            Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        //去环信服务器 告诉服务器， 拒绝别人申请入群
                        EMClient.getInstance().groupManager().declineApplication(invationInfo.getGroupInfo().getInvatePerson(),
                                invationInfo.getGroupInfo().getGroupId(),
                                "你是小黑子，不要来我们Ikun群");
                        //更新本地数据库
                        invationInfo.setStatus(InvationInfo.InvitationStatus.GROUP_REJECT_APPLICATION);
                        Model.getInstance().getDbManager().getInviteTableDao().addInvitation(invationInfo);
                        //更新内存数据
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ToastUtil.show(InviteActivity.this,"拒绝了小黑子加群了");
                                //刷新页面
                                refresh();
                            }
                        });
                    } catch (HyphenateException e) {
                        e.printStackTrace();
                        ToastUtil.show(InviteActivity.this, "发生了错误！");
                    }
                }
            });

        }
    };

    private BroadcastReceiver InviteChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //邀请信息变化了，我就刷新页面
            refresh();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);

        initView();
        initData();
    }

    private void initData() {
        //初始化listView
        adapter = new InviteAdapter(this, mOnInviteListener);
        lv_invite.setAdapter(adapter);

        //刷新方法
        refresh();

        //注册邀请信息变化的广播
        mLBM = LocalBroadcastManager.getInstance(this);
        mLBM.registerReceiver(InviteChangedReceiver, new IntentFilter(Constant.CONTACTS_INVITE_CHANGED));
        mLBM.registerReceiver(InviteChangedReceiver, new IntentFilter(Constant.GROUP_INVITE_CHANGED));
    }

    private void refresh() {
        //获取数据库中的所有邀请信息
        List<InvationInfo> invitations = Model.getInstance().getDbManager().getInviteTableDao().getInvitations();
        //刷新适配器
        adapter.refresh(invitations);
    }

    private void initView() {
        lv_invite = findViewById(R.id.lv_invite);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLBM.unregisterReceiver(InviteChangedReceiver);
    }
}