package com.hcb.im.controller.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hcb.im.R;
import com.hcb.im.model.Model;
import com.hcb.im.model.entity.Constant;
import com.hcb.im.model.entity.UserInfo;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.constants.EaseConstant;
import com.hyphenate.easeui.modules.chat.EaseChatFragment;
import com.hyphenate.easeui.modules.chat.interfaces.OnChatLayoutListener;
import com.hyphenate.easeui.ui.base.EaseBaseActivity;
import com.hyphenate.easeui.utils.EaseCommonUtils;
import com.hyphenate.easeui.widget.EaseImageView;

import org.w3c.dom.Text;

//会话详情页面
public class ChatActivity extends AppCompatActivity {

    private String mHxid;
    private EaseChatFragment easeChatFragment;
    private ImageView iv_chat_detail;
    private TextView tv_chat_title;
    private String mGroupName;
    private int chatType;
    private LocalBroadcastManager mLBM;
    private int mChatType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        initData();

        initView();

        initListener();
    }

    private void initView() {
        iv_chat_detail = findViewById(R.id.iv_chat_detail);
        tv_chat_title = findViewById(R.id.tv_chat_title);
        if (chatType == EaseConstant.CHATTYPE_GROUP){
            tv_chat_title.setText(mGroupName);
        }else{
            tv_chat_title.setText(mHxid);
            iv_chat_detail.setVisibility(View.GONE);
        }

    }

    private void initListener() {
        iv_chat_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChatActivity.this, GroupDetailActivity.class);
                intent.putExtra(Constant.GROUP_ID, mHxid);
                startActivity(intent);
            }
        });

        //如果当前类型为群聊
        if (chatType == EaseConstant.CHATTYPE_GROUP){
            // 注册退群广播
            BroadcastReceiver ExitGroupReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    //结束当前页面
                    finish();
                }
            };
            //注册退群广播
            mLBM.registerReceiver(ExitGroupReceiver,new IntentFilter(Constant.QUIT_GROUP));
        }
    }



    private void initData() {
        //创建一个会话的fragment
        easeChatFragment = new EaseChatFragment();
        mHxid = getIntent().getStringExtra(EaseConstant.EXTRA_CONVERSATION_ID);
        mGroupName = getIntent().getStringExtra("group_name");
        chatType = getIntent().getIntExtra(EaseConstant.EXTRA_CHAT_TYPE, 1);

        //获取聊天类型的参数
        mChatType = getIntent().getExtras().getInt(EaseConstant.EXTRA_CHAT_TYPE);

        easeChatFragment.setArguments(getIntent().getExtras());

        //替换fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fl_container, easeChatFragment).commit();

        //获取发送广播的管理者
        mLBM = LocalBroadcastManager.getInstance(ChatActivity.this);
    }
}