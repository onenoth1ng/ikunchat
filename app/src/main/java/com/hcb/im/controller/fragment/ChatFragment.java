package com.hcb.im.controller.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.hcb.im.R;
import com.hcb.im.controller.activity.AddContactActivity;
import com.hcb.im.controller.activity.ChatActivity;
import com.hcb.im.utils.ToastUtil;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.constants.EaseConstant;
import com.hyphenate.easeui.modules.conversation.EaseConversationListFragment;
import com.hyphenate.easeui.modules.conversation.delegate.EaseSystemMsgDelegate;
import com.hyphenate.easeui.utils.EaseCommonUtils;
import com.hyphenate.easeui.widget.EaseImageView;
import com.hyphenate.easeui.widget.EaseSearchTextView;

import java.util.List;

//创建会话列表页面
public class ChatFragment extends EaseConversationListFragment {
    private EaseSearchTextView easeSearchTextView;
    //private LocalBroadcastManager mLBM;
    public static final String BROADCAST_ACTION_DISC = "BROADCAST_ACTION_DISC";
    private BroadcastReceiver ConversationChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //收到广播之后刷新UI
            conversationListLayout.loadDefaultData();
        }
    };


    @Override
    public void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);

        EMMessageListener msgListener = new EMMessageListener() {

            // 收到消息，遍历消息队列，解析和显示。
            @Override
            public void onMessageReceived(List<EMMessage> messages) {
                //接收到消息，发送广播
                getActivity().sendBroadcast(new Intent(ChatFragment.BROADCAST_ACTION_DISC));
            }
        };
        EMClient.getInstance().chatManager().addMessageListener(msgListener);

        //添加搜索会话布局
        View view = LayoutInflater.from(mContext).inflate(R.layout.conversation_search, null);
        llRoot.addView(view, 0);
        easeSearchTextView = view.findViewById(R.id.tv_search);

        //mLBM = LocalBroadcastManager.getInstance(getContext());
        getContext().registerReceiver(ConversationChangedReceiver, new IntentFilter(BROADCAST_ACTION_DISC));


    }

    @Override
    public void initListener() {
        super.initListener();
        easeSearchTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ToastUtil.show(mContext, "别点我小黑子，懒得写搜索功能");
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        super.onItemClick(view, position);
        Object item = conversationListLayout.getItem(position).getInfo();
        if (item instanceof EMConversation) {
            //跳转到聊天界面之后刷新UI
            conversationListLayout.loadDefaultData();
            //创建跳转意图
            Intent intent = new Intent(mContext, ChatActivity.class);
            //传递参数（具体是哪个联系人 是单聊还是群聊）
            intent.putExtra(EaseConstant.EXTRA_CONVERSATION_ID, ((EMConversation) item).conversationId());
            intent.putExtra(EaseConstant.EXTRA_CHAT_TYPE, 1);
            mContext.startActivity(intent);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getContext().unregisterReceiver(ConversationChangedReceiver);
    }
}
