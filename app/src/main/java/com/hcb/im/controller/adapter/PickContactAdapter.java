package com.hcb.im.controller.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.hcb.im.R;
import com.hcb.im.model.entity.PickContactInfo;
import com.hcb.im.model.entity.UserInfo;

import java.util.ArrayList;
import java.util.List;

//选择联系人的适配器
public class PickContactAdapter extends BaseAdapter {
    private Context mContext;
    private List<PickContactInfo> mPicks = new ArrayList<>();
    private List<String > mExistMembers = new ArrayList<>();//保存群里面已经存在的成员名字集合

    public PickContactAdapter(Context context, List<PickContactInfo> picks, List<String> exitsMembers) {
        mContext = context;
        if (picks != null && picks.size() >= 0){
            mPicks.clear();
            mPicks.addAll(picks);
        }

        //加载已经存在的群成员集合
        mExistMembers.clear();
        mExistMembers.addAll(exitsMembers);
    }

    @Override
    public int getCount() {
        return mPicks == null ? 0 : mPicks.size();
    }

    @Override
    public Object getItem(int i) {
        return mPicks.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        //创建 或 获取 ViewHolder
        ViewHolder holder = null;

        if (convertView == null){
            holder = new ViewHolder();

            convertView = View.inflate(mContext, R.layout.item_pick,null);
            holder.cb = convertView.findViewById(R.id.cb_item_pick_contacts);
            holder.tv_name = convertView.findViewById(R.id.tv_item_pick_contacts_name);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        //获取当前item数据
        PickContactInfo pickContactInfo = mPicks.get(i);
        //展现item数据
        holder.tv_name.setText(pickContactInfo.getUserInfo().getName());
        holder.cb.setChecked(pickContactInfo.isChecked());

        //判断，群成员是否存在
        if (mExistMembers.contains(pickContactInfo.getUserInfo().getHxid())){
            //更新页面
            holder.cb.setChecked(true);
            //更新数据
            pickContactInfo.setChecked(true);
        }

        //返回convertView
        return convertView;
    }

    // 获取选择的联系人
    public List<String> getPickContacts() {
        List<String> picks = new ArrayList<>();
        for (PickContactInfo pick : mPicks) {
            // 判断是否选中
            if (pick.isChecked()){
                picks.add(pick.getUserInfo().getName());
            }
        }

        return picks;
    }

    private class ViewHolder{
        private CheckBox cb;
        private TextView tv_name;
    }
}
