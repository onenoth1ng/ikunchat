package com.hcb.im.controller.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.hcb.im.R;
import com.hcb.im.controller.adapter.PickContactAdapter;
import com.hcb.im.model.Model;
import com.hcb.im.model.entity.Constant;
import com.hcb.im.model.entity.PickContactInfo;
import com.hcb.im.model.entity.UserInfo;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMGroup;
import com.hyphenate.exceptions.HyphenateException;

import java.util.ArrayList;
import java.util.List;

public class PickContactActivity extends AppCompatActivity {
    private TextView tv_pick_contacts_save;
    private ListView lv_pick_contacts;
    private List<PickContactInfo> mPicks;
    private List<UserInfo> contacts;
    private PickContactAdapter pickContactAdapter;
    private List<String> mExitMembers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_contact);

        //获取传递过来的数据
        getData();

        initView();

        initData();

        initListener();
    }

    private void getData() {
        String groupId = getIntent().getStringExtra(Constant.GROUP_ID);
        if (groupId != null){
            //群里要拉人
            EMGroup group = EMClient.getInstance().groupManager().getGroup(groupId);
            //把群里面存在的所有群成员获取到
            mExitMembers = group.getMembers();
        }

        if (mExitMembers == null){
            mExitMembers = new ArrayList<>();
        }
    }

    private void initListener() {
        //listView条目的点击事件
        lv_pick_contacts.setOnItemClickListener((adapterView, view, i, l) -> {
            //checkBox切换
            CheckBox cb_pick = view.findViewById(R.id.cb_item_pick_contacts);
            cb_pick.setChecked(!cb_pick.isChecked());

            //修改数据
            PickContactInfo pickContactInfo = mPicks.get(i);
            pickContactInfo.setChecked(cb_pick.isChecked());

            //刷新页面
            pickContactAdapter.notifyDataSetChanged();
        });

        //保存按钮的点击事件处理
        tv_pick_contacts_save.setOnClickListener(view -> {
            //获取到已经选择的联系人
            List<String> pickNames = pickContactAdapter.getPickContacts();
            //给启动页面返回数据
            Intent intent = new Intent();
            for (String pickName : pickNames) {
                Log.d("OneNoth1ng", "选了："+pickName);
            }
            intent.putExtra("members",pickNames.toArray(new String[0]));
            setResult(RESULT_OK,intent);
            //结束当前页面
            finish();
        });
    }

    private void initData() {
        //从本地联系人获取所有的联系人信息
        contacts = Model.getInstance().getDbManager().getContactTableDao().getContacts();
        mPicks = new ArrayList<>();
        if (contacts != null && contacts.size() >= 0){
            //转换到选择联系人需要的视图类
            for (UserInfo contact : contacts) {
                PickContactInfo pickContactInfo = new PickContactInfo(contact,false);
                mPicks.add(pickContactInfo);
            }
        }
        //初始化listView
        pickContactAdapter = new PickContactAdapter(this, mPicks,mExitMembers);
        lv_pick_contacts.setAdapter(pickContactAdapter);
    }

    private void initView() {
        tv_pick_contacts_save = findViewById(R.id.tv_pick_contacts_save);
        lv_pick_contacts =  findViewById(R.id.lv_pick_contacts);
    }
}