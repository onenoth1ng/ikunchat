package com.hcb.im.controller.activity;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.hcb.im.R;
import com.hcb.im.controller.adapter.GroupDetailAdapter;
import com.hcb.im.model.Model;
import com.hcb.im.model.entity.Constant;
import com.hcb.im.model.entity.UserInfo;
import com.hcb.im.utils.ToastUtil;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMGroup;
import com.hyphenate.exceptions.HyphenateException;

import java.util.ArrayList;
import java.util.List;

public class GroupDetailActivity extends AppCompatActivity {

    private String groupId;
    private EMGroup emGroup;
    private Button bt_groupdetail_out;
    private GridView gv_groupdetail;
    private ActivityResultLauncher<Intent> register;
    private GroupDetailAdapter.OnGroupDetailListener mOnGroupDetailListener = new GroupDetailAdapter.OnGroupDetailListener() {
        //群里拉人
        @Override
        public void onAddMembers() {
            Intent intent = new Intent(GroupDetailActivity.this,PickContactActivity.class);
            intent.putExtra(Constant.GROUP_ID,emGroup.getGroupId());
            register.launch(intent);
        }
        //群里删人
        @Override
        public void onDeleteMembers(UserInfo user) {
            Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        //从环信服务器删除此人
                        EMClient.getInstance().groupManager().removeUserFromGroup(emGroup.getGroupId(),user.getHxid());
                        //更新页面
                        getMembersFromHxServer();
                        //谈土司
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ToastUtil.show(GroupDetailActivity.this,"删除"+user.getHxid()+"成功");
                            }
                        });
                    } catch (HyphenateException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
    private GroupDetailAdapter groupDetailAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_detail);

        //跳转到选择联系人然后回来 的 回调方法(群详情的加好友方法)
        register = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            Intent data = result.getData();
            //得到选择了那些联系人  的 信息
            try {
                Model.getInstance().getGlobalThreadPool().execute(() -> {
                    try {
                        String[] members = data.getStringArrayExtra("members");
                        Log.d("OneNoth1ng", "群组选人的长度是："+members.length);
                        EMClient.getInstance().groupManager().addUsersToGroup(emGroup.getGroupId(),members);

                        //弹Toast
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ToastUtil.show(GroupDetailActivity.this,"发送邀请成功");
                            }
                        });
                    } catch (HyphenateException e) {
                        e.printStackTrace();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ToastUtil.show(GroupDetailActivity.this,"未知错误" + e.toString());
                            }
                        });
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }
        });

        initView();
        getData();

        initData();
        initListener();
    }


    //获取传递过来的数据(群Id,查询去群的所有数据)
    private void getData() {
        groupId = getIntent().getStringExtra(Constant.GROUP_ID);
        if (groupId == null){
            return;
        }else {
            emGroup = EMClient.getInstance().groupManager().getGroup(groupId);

        }
    }

    //初始化数据
    private void initData() {
        //初始化Button显示
        initButtonDisplay();

        //初始化GridView网格布局
        initGridView();

        //从环信服务器获取所有的群成员
        getMembersFromHxServer();
    }

    //从环信服务器获取所有的群成员
    private void getMembersFromHxServer() {
        Model.getInstance().getGlobalThreadPool().execute(new Runnable() {

            private List<UserInfo> mUsers;

            @Override
            public void run() {
                try {
                    EMGroup group = EMClient.getInstance().groupManager().getGroupFromServer(emGroup.getGroupId());
                    List<String> members = group.getMembers();
                    mUsers = new ArrayList<>();
                    if (members != null && members.size() >= 0 ){
                        for (String member : members) {
                            UserInfo userInfo = new UserInfo(member);
                            mUsers.add(userInfo);
                        }
                    }
                    //更新页面
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //刷新适配器
                            groupDetailAdapter.refresh(mUsers);
                        }
                    });

                } catch (HyphenateException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ToastUtil.show(GroupDetailActivity.this,"出未知错误了" + e);
                        }
                    });
                }
            }
        });
    }

    //初始化网格布局
    private void initGridView() {
        //true表示是群主，或者这个群是 公开群  【群详情可以被修改】
        //true表示非群组，或者群 非公开
        boolean isCanModify = EMClient.getInstance().getCurrentUser().equals(emGroup.getOwner()) || (emGroup.isPublic());
        groupDetailAdapter = new GroupDetailAdapter(this,isCanModify, mOnGroupDetailListener);
        gv_groupdetail.setAdapter(groupDetailAdapter);
    }

    //初始化Button显示
    private void initButtonDisplay() {
        //当前用户为群主
        if (EMClient.getInstance().getCurrentUser().equals(emGroup.getOwner())){
            //群主的处理
            bt_groupdetail_out.setText("解散群");
            bt_groupdetail_out.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                EMClient.getInstance().groupManager().destroyGroup(groupId);
                                //发送退群的广播
                                quitGroupBroadcast();
                                //更新页面
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ToastUtil.show(GroupDetailActivity.this,"再见了小黑子们~~");
                                        finish();
                                    }
                                });
                            } catch (HyphenateException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });
        }else{
            //刁民的处理
            bt_groupdetail_out.setText("退群");
            bt_groupdetail_out.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                //告诉环信服务器我退群了
                                EMClient.getInstance().groupManager().leaveGroup(groupId);
                                //发送退群的广播
                                quitGroupBroadcast();
                                //更新页面
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ToastUtil.show(GroupDetailActivity.this,"还会再见面吗小黑子~~");
                                        finish();
                                    }
                                });
                            } catch (HyphenateException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });
        }
    }

    //发送退群 和 解散群 的广播
    private void quitGroupBroadcast() {
        LocalBroadcastManager mLBM = LocalBroadcastManager.getInstance(GroupDetailActivity.this);

        Intent intent = new Intent(Constant.QUIT_GROUP);
        intent.putExtra(Constant.QUIT_GROUP, emGroup.getGroupId());
        mLBM.sendBroadcast(intent);

    }

    //这里的监听是 按住了群详情页面的其他位置，就推出删除模式
    private void initListener() {
        gv_groupdetail.setOnTouchListener((view, motionEvent) -> {
            switch (motionEvent.getAction()){
                case MotionEvent.ACTION_DOWN:
                    //判断当前是否是删除模式
                    if (groupDetailAdapter.ismIsDeleteModel()){//是删除模式
                        //切换到非删除模式
                        groupDetailAdapter.setmIsDeleteModel(false);
                        //刷新页面
                        groupDetailAdapter.notifyDataSetChanged();
                    }
                    break;
            }
            return false;
        });

    }


    private void initView() {
        bt_groupdetail_out = findViewById(R.id.bt_groupdetail_out);
        gv_groupdetail = findViewById(R.id.gv_groupdetail);

    }



}