package com.hcb.im.controller.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AlertDialogLayout;

import com.hcb.im.R;
import com.hcb.im.controller.activity.ChatActivity;
import com.hcb.im.controller.activity.MainActivity;
import com.hcb.im.controller.fragment.ChatFragment;
import com.hcb.im.model.Model;
import com.hcb.im.model.entity.InvationInfo;
import com.hcb.im.model.entity.UserInfo;
import com.hcb.im.utils.ToastUtil;
import com.hyphenate.chat.EMClient;
import com.hyphenate.easeui.constants.EaseConstant;
import com.hyphenate.exceptions.HyphenateException;

import java.util.ArrayList;
import java.util.List;

public class ContactsAdapter extends BaseAdapter {

    private Context mContext;
    private List<UserInfo> mUserInfos = new ArrayList<>();


    public ContactsAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return mUserInfos == null ? 0 : mUserInfos.size();
    }

    @Override
    public Object getItem(int i) {
        return mUserInfos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        //1.获取或创建ViewHolder
        ViewHolder holder = new ViewHolder();
        if (convertView == null){
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.item_contact,null);
            holder.name = convertView.findViewById(R.id.tv_contact_name);
            holder.photo = convertView.findViewById(R.id.iv_contact_photo);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        //2.获取当前item数据
        UserInfo userInfo = mUserInfos.get(i);
        //3.显示当前的item数据
        if (userInfo != null){
            holder.name.setText(userInfo.getName());
        }
        //点击联系人跳转到聊天页面
        convertView.findViewById(R.id.ll_contact_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ChatActivity.class);
                //传递参数
                intent.putExtra(EaseConstant.EXTRA_CONVERSATION_ID,userInfo.getHxid());
                intent.putExtra(EaseConstant.EXTRA_CHAT_TYPE,1);
                mContext.startActivity(intent);
            }
        });

        convertView.findViewById(R.id.ll_contact_item).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("确认删除联系人?");
                builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //删除联系人
                        Model.getInstance().getGlobalThreadPool().execute(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    EMClient.getInstance().contactManager().deleteContact(userInfo.getHxid());
                                } catch (HyphenateException e) {
                                    e.printStackTrace();
                                    //ToastUtil.show(mContext,"删除失败");
                                }
                            }
                        });
                        //本地数据库的更新
                        Model.getInstance().getDbManager().getContactTableDao().deleteContactByHxId(userInfo.getHxid());
                        ToastUtil.show(mContext,"删除成功");
                    }
                });
                builder.setNegativeButton("取消",null);
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                return true;
            }
        });


        return convertView;
    }


    private class ViewHolder{
        private TextView name;
        private ImageView photo;
    }



    //刷新数据的方法
    public void refresh(List<UserInfo> userInfos){

        if (userInfos != null && userInfos.size() >= 0){
            mUserInfos.addAll(userInfos);
            notifyDataSetChanged();
        }
    }
}
