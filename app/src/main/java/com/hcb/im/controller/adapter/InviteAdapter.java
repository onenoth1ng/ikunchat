package com.hcb.im.controller.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.hcb.im.R;
import com.hcb.im.model.entity.InvationInfo;
import com.hcb.im.model.entity.UserInfo;

import java.util.ArrayList;
import java.util.List;


//邀请信息列表的适配器
public class InviteAdapter extends BaseAdapter {
    private Context mContext;
    private List<InvationInfo> mInvatationInfos = new ArrayList<>();
    private OnInviteListener mOnInviteListener;

    private InvationInfo invationInfo;
    public InviteAdapter(Context context, OnInviteListener OnInviteListener) {
        mContext = context;
        mOnInviteListener = OnInviteListener;
    }

    //刷新数据的方法
    public void refresh(List<InvationInfo> invationInfos){

        if (invationInfos != null && invationInfos.size() >= 0){
            mInvatationInfos.clear();
            mInvatationInfos.addAll(invationInfos);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        return mInvatationInfos == null ? 0 : mInvatationInfos.size();
    }

    @Override
    public Object getItem(int i) {
        return mInvatationInfos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        //1.获取或创建ViewHolder
        ViewHolder holder = new ViewHolder();
        if (convertView == null){
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.item_invite,null);
            holder.name = convertView.findViewById(R.id.tv_invite_name);
            holder.reason = convertView.findViewById(R.id.tv_invite_reason);
            holder.accept = convertView.findViewById(R.id.bt_invite_accept);
            holder.reject = convertView.findViewById(R.id.bt_invite_reject);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        //2.获取当前的item数据
        invationInfo = mInvatationInfos.get(i);
        //3.显示当前的item数据
        UserInfo userInfo = invationInfo.getUserInfo();
        if (userInfo != null){//当前是联系人的邀请信息
            //名称的展示
            holder.name.setText(invationInfo.getUserInfo().getName());
            //接受拒绝按钮默认拒绝了，后期开启
            holder.accept.setVisibility(View.GONE);
            holder.reject.setVisibility(View.GONE);
            //邀请加你 或者是  加成功了/加失败了
            if (invationInfo.getStatus() == InvationInfo.InvitationStatus.NEW_INVITE){//别人要加你为好友
                if (invationInfo.getReason() == null){
                    holder.reason.setText("小黑子请求添加你为好友");
                }else{
                    holder.reason.setText(invationInfo.getReason());
                }
                holder.accept.setVisibility(View.VISIBLE);
                holder.reject.setVisibility(View.VISIBLE);
            }else if (invationInfo.getStatus() == InvationInfo.InvitationStatus.INVITE_ACCEPT){//需要你接受邀请
                if (invationInfo.getReason() == null){
                    holder.reason.setText("小黑子接受了你的邀请");
                }else{
                    holder.reason.setText(invationInfo.getReason());
                }
            }else if (invationInfo.getStatus() == InvationInfo.InvitationStatus.INVITE_ACCEPT_BY_PEER){//邀请被接受
                if (invationInfo.getReason() == null){
                    holder.reason.setText("小黑子接受了你的邀请");
                }else{
                    holder.reason.setText(invationInfo.getReason());
                }
            }
            //接受按钮点击事件的处理
            holder.accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnInviteListener.onAccept(invationInfo);
                }
            });

            //拒绝按钮点击事件的处理
            holder.reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnInviteListener.onReject(invationInfo);
                }
            });

        }else {
            //应该是群组的邀请信息
            //显示名称
            holder.name.setText(invationInfo.getGroupInfo().getInvatePerson() + "的群邀请");

            //显示原因
            holder.accept.setVisibility(View.GONE);
            holder.reject.setVisibility(View.GONE);

            // 显示原因
            switch(invationInfo.getStatus()){
                // 您的群申请已经被接受
                case GROUP_APPLICATION_ACCEPTED:
                    holder.reason.setText("您的群申请请已经被接受");
                    break;
                //  您的群邀请已经被接收
                case GROUP_INVITE_ACCEPTED:
                    holder.reason.setText("您的群邀请已经被接受");
                    break;

                // 你的群申请已经被拒绝
                case GROUP_APPLICATION_DECLINED:
                    holder.reason.setText("你的群申请已经被拒绝");
                    break;

                // 您的群邀请已经被拒绝
                case GROUP_INVITE_DECLINED:
                    holder.reason.setText("您的群邀请已经被拒绝");
                    break;

                // 您收到了群邀请
                case NEW_GROUP_INVITE:
                    holder.accept.setVisibility(View.VISIBLE);
                    holder.reject.setVisibility(View.VISIBLE);

                    // 接受邀请 按钮 的点击事件
                    holder.accept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mOnInviteListener.onInviteAccept(invationInfo);
                        }
                    });

                    // 拒绝邀请 按钮 的点击事件
                    holder.reject.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mOnInviteListener.onInviteReject(invationInfo);
                        }
                    });

                    holder.reason.setText(invationInfo.getGroupInfo().getInvatePerson() + "邀请你加入" + invationInfo.getGroupInfo().getGroupName());
                    break;

                // 您收到了群申请
                case NEW_GROUP_APPLICATION:
                    holder.accept.setVisibility(View.VISIBLE);
                    holder.reject.setVisibility(View.VISIBLE);

                    // 接受 群申请 按钮 的点击事件
                    holder.accept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mOnInviteListener.onApplicationAccept(invationInfo);
                        }
                    });

                    // 拒绝 群申请 按钮 的点击事件
                    holder.reject.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mOnInviteListener.onApplicationReject(invationInfo);
                        }
                    });

                    holder.reason.setText(invationInfo.getGroupInfo().getInvatePerson() + "想要加入" + invationInfo.getGroupInfo().getGroupName());
                    break;

                // 你接受了群邀请
                case GROUP_ACCEPT_INVITE:
                    holder.reason.setText("你接受了群邀请");
                    break;

                // 您批准了群申请
                case GROUP_ACCEPT_APPLICATION:
                    holder.reason.setText("您批准了群申请");
                    break;

                // 您拒绝了群邀请
                case GROUP_REJECT_INVITE:
                    holder.reason.setText("您拒绝了群邀请");
                    break;

                // 您拒绝了群申请
                case GROUP_REJECT_APPLICATION:
                    holder.reason.setText("您拒绝了群申请");
                    break;
            }
        }
        //4.返回view
        return convertView;
    }

    private class ViewHolder{
        private TextView name;
        private TextView reason;
        private Button accept;
        private Button reject;
    }

    public interface OnInviteListener{
        //联系人接受按钮的点击事件
        void onAccept(InvationInfo invationInfo);
        //联系人拒绝按钮的点击事件
        void onReject(InvationInfo invationInfo);

        //接受邀请按钮的处理
        void onInviteAccept(InvationInfo invationInfo);
        //拒绝邀请按钮的处理
        void onInviteReject(InvationInfo invationInfo);
        //接受别人申请入群按钮的处理
        void onApplicationAccept(InvationInfo invationInfo);
        //拒绝别人申请入群按钮的处理
        void onApplicationReject(InvationInfo invationInfo);

    }
}
