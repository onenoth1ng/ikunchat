package com.hcb.im.controller.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.hcb.im.R;
import com.hcb.im.controller.activity.AddContactActivity;
import com.hcb.im.controller.activity.GroupListActivity;
import com.hcb.im.controller.activity.InviteActivity;
import com.hcb.im.controller.adapter.ContactsAdapter;
import com.hcb.im.model.Model;
import com.hcb.im.model.entity.Constant;
import com.hcb.im.model.entity.UserInfo;
import com.hcb.im.utils.SpUtils;
import com.hyphenate.chat.EMClient;
import com.hyphenate.easeui.modules.contact.EaseContactListFragment;
import com.hyphenate.easeui.widget.EaseTitleBar;
import com.hyphenate.exceptions.HyphenateException;

import java.util.ArrayList;
import java.util.List;

//联系人列表页面
public class ContactListFragment extends Fragment {

    private ImageView iv_redpoint_user;
    private LocalBroadcastManager mLBM;
    private View view;
    private BroadcastReceiver ContactInviteChangeReceiver = new BroadcastReceiver() {
        //接受到了广播
        @Override
        public void onReceive(Context context, Intent intent) {
            //更新红点显示
            iv_redpoint_user.setVisibility(View.VISIBLE);
            SpUtils.getInstance().save(SpUtils.IS_NEW_INVITE, true);
        }
    };
    private LinearLayout ll_contact_invite;
    private BroadcastReceiver ContactChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshContacts();
        }
    };
    private String mHxid;

    private BroadcastReceiver GroupChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //显示红点
            iv_redpoint_user.setVisibility(View.VISIBLE);
            SpUtils.getInstance().save(SpUtils.IS_NEW_INVITE, true);
        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        initView();
        // 根据布局文件 fragment_contacts.xml创建视图对象
        view = inflater.inflate(R.layout.fragment_contacts, container, false);

        EaseTitleBar title_bar = view.findViewById(R.id.title_bar);
        title_bar.setRightImageResource(R.drawable.em_add);

        //添加联系人的点击事件处理
        title_bar.setRightLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AddContactActivity.class);
                startActivity(intent);
            }
        });

        iv_redpoint_user = view.findViewById(R.id.iv_redpoint_user);
        boolean isNewInvite = SpUtils.getInstance().getBoolean(SpUtils.IS_NEW_INVITE, false);
        iv_redpoint_user.setVisibility(isNewInvite ? View.VISIBLE : View.GONE);

        //给好友邀请条目添加点击事件
        view.findViewById(R.id.ll_contact_invite_user).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //消除小红点
                iv_redpoint_user.setVisibility(View.GONE);
                SpUtils.getInstance().save(SpUtils.IS_NEW_INVITE, false);
                //跳转到邀请信息列表页面
                Intent intent = new Intent(getActivity(), InviteActivity.class);
                startActivity(intent);
            }
        });

        //给群组添加点击事件，跳转到群组页面
        view.findViewById(R.id.ll_contact_invite_group).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), GroupListActivity.class);
                startActivity(intent);
            }
        });


        //注册广播
        mLBM = LocalBroadcastManager.getInstance(getActivity());
        mLBM.registerReceiver(ContactInviteChangeReceiver, new IntentFilter(Constant.CONTACTS_INVITE_CHANGED));
        mLBM.registerReceiver(ContactChangeReceiver, new IntentFilter(Constant.CONTACTS_CHANGED));
        mLBM.registerReceiver(GroupChangeReceiver,new IntentFilter(Constant.GROUP_INVITE_CHANGED));
        //从环信服务器获取所有的联系人信息
        getContactFromHxServer();


        return view;
    }

    //从环信服务器获取所有的联系人信息
    private void getContactFromHxServer() {
        Model.getInstance().getGlobalThreadPool().execute(() -> {
            try {
                //获取所有好友的环信id
                List<String> hxids = EMClient.getInstance().contactManager().getAllContactsFromServer();
                if (hxids != null && hxids.size() >= 0){
                    List<UserInfo> contacts = new ArrayList<UserInfo>();
                    for (String hxid : hxids) {
                        UserInfo userInfo = new UserInfo(hxid);
                        contacts.add(userInfo);
                    }
                    //保存好友信息到本地数据库
                    Model.getInstance().getDbManager().getContactTableDao().saveContacts(contacts,true);

                    if (getActivity() == null){
                        return;
                    }
                    //刷新页面
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //刷新页面
                            refreshContacts();
                        }
                    });
                }
            } catch (HyphenateException e) {
                e.printStackTrace();
            }
        });

    }

    //这个方法是  联系人列表的刷出来
    private void refreshContacts() {
        //获取数据
        List<UserInfo> contacts = Model.getInstance().getDbManager().getContactTableDao().getContacts();

        //校验
        if (contacts != null && contacts.size() >= 0){
            //创建联系人listView列表的构造器
            ContactsAdapter adapter = new ContactsAdapter(getActivity());
            //刷新联系人的数据
            adapter.refresh(contacts);
            ListView lv_contact = view.findViewById(R.id.lv_contact);
            lv_contact.setAdapter(adapter);

        }
    }

    private void initView() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLBM.unregisterReceiver(ContactInviteChangeReceiver);
        mLBM.unregisterReceiver(ContactChangeReceiver);
        mLBM.unregisterReceiver(GroupChangeReceiver);
    }
}
